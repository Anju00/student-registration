<!DOCTYPE html>
<html>
<head>

    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        th {
            background-color: #f2f2f2;
        }

        input[type="text"], select {
            width: 100%;
            padding: 6px 10px;
            margin: 4px 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }

        input[type="submit"] {
            background-color: #4CAF50;
            color: white;
            padding: 8px 12px;
            margin-top: 10px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>
    <table>
      <tr>
          <th>First name</th>
          <th>Last name</th>
          <th>email</th>
          <th>Date of birth</th>
          <th>course</th>
      </tr>
        @foreach($student as $student)
            <tr>
                <td>{{$student->id}}</td>
                <td>{{$student->first_name}}</td>
                <td>{{$student->Last_name}}</td>
                <td>{{$student->email}}</td>
                <td>{{$student->date_of_birthday}}</td>
                <td>{{$student->course_name}}</td>
            </tr>
        @endforeach

    </table>

</body>
</html>
