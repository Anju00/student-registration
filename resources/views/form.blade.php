<html>
<head>
    <title>Student Registration Form</title>
    <style>
        .container {
            width: 400px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            text-align: center;
        }

        .container label {
            font-weight: bold;
        }

        .container input[type=text], .container select {
            width: 100%;
            padding: 12px;
            margin: 8px 0;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        .container button[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        .container button[type=submit]:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>
<div class="container">
    <h2>Student Registration Form</h2>
    <form action="/students" method="POST">
        @csrf
        <label for="Fname">First Name:</label>
        <input type="text" id="fname" name="fname" placeholder="Enter your name" required>

        <label for="lname">Last Name:</label>
        <input type="text" id="lname" name="lname" placeholder="Enter your name" required>


        <label for="email">Email:</label>
        <input type="text" id="email" name="email" placeholder="Enter your email" required>

        <label for="dob">Date of Birth:</label>
        <input type="text" id="dob" name="dob" placeholder="Enter your date of birth" required>


        <label for="course">Course</label>
        <select id="course_id" name="course_id">
            @foreach($courses as $course)
                <option value="{{$course->id}}">{{$course->name}}</option>
            @endforeach
        </select>

        <button type="submit">Submit</button>
    </form>
</div>
</body>
</html>


