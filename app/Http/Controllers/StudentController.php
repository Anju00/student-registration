<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\Course;

class  StudentController extends Controller
{


    public function index()
    {
        $students = Student::all();
        return view('student', ['student' => $students]);

    }

    public function create()
    {
        $courses = Course::all();
        return view('form', [
            'courses' => $courses

        ]);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:student',
            'date_of_birth' => 'required|date',
            'course_id' => 'required|exists:course,id',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        // Data is valid, proceed with storing the student record

        Student::create($request->all());

        return redirect('/')->with('success', 'Student registered successfully!');
    }
}
