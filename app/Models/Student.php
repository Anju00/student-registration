<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class student extends Model
{
    use HasFactory;

    protected $table='students';

    protected $fillable = ['first name', 'last name','email', 'dob'];


    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
