<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert([
            'id' => 1,
            'name' => 'Software Engineering',
        ]);
        DB:: table('courses')->insert([
            'id' => 2,
            'name' => 'computer science',
        ]) ;
    }
}
